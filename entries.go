package oxford

import (
	"fmt"
)

type EntryService service

type RetrieveEntry struct {
	Metadata *Metadata        `json:"metadata,omitempty"`
	Results  []*HeadwordEntry `json:"results"`
}

type Metadata struct {
	Provider string `json:"provider"`
}

type HeadwordEntry struct {
	ID             string           `json:"id"`
	Language       string           `json:"language"`
	LexicalEntries []*LexicalEntry  `json:"lexicalEntries"`
	Pronunciations []*Pronunciation `json:"pronunciations"`
	Type           string           `json:"type"`
	Word           string           `json:"word"`
}

type LexicalEntry struct {
	DerivativeOf        []*RelatedEntry       `json:"derivativeOf"`
	Entries             []*Entry              `json:"entries"`
	GrammaticalFeatures []*GrammaticalFeature `json:"grammaticalFeatures"`
	Language            string                `json:"language"`
	LexicalCategory     string                `json:"lexicalCategory"`
	Notes               []*CategorizedText    `json:"notes"`
	Pronunciations      []*Pronunciation      `json:"pronunciations"`
	Text                string                `json:"text"`
	VariantForms        []*VariantForm        `json:"variantForms"`
}

type Pronunciation struct {
	AudioFile        string   `json:"type"`
	Dialects         string   `json:"url"`
	PhoneticNotation string   `json:"phoneticNotation"`
	PhoneticSpelling string   `json:"phoneticSpelling"`
	Regions          []string `json:"regions"`
}

type RelatedEntry struct {
	Domains   []string `json:"domains"`
	ID        string   `json:"id"`
	Language  string   `json:"string"`
	Regions   []string `json:"regions"`
	Registers []string `json:"registers"`
	Text      string   `json:"text"`
}

type Entry struct {
	Etymologies         []string              `json:"etymologies"`
	GrammaticalFeatures []*GrammaticalFeature `json:"grammaticalFeatures"`
	HomographNumber     string                `json:"homographNumber"`
	Notes               []*CategorizedText    `json:"notes"`
	Pronunciations      []*Pronunciation      `json:"pronunciations"`
	Senses              []*Sense              `json:"senses"`
	VariantForms        []*VariantForm        `json:"variantForms"`
}

type GrammaticalFeature struct {
	Text string `json:"text"`
	Type string `json:"type"`
}

type CategorizedText struct {
	ID   string `json:"id"`
	Text string `json:"text"`
	Type string `json:"type"`
}

type Sense struct {
	CrossReferenceMarkers []string           `json:"crossReferenceMarkers"`
	CrossReference        []*CrossReference  `json:"crossReferences"`
	Definitions           []string           `json:"definitions"`
	Domains               []string           `json:"domains"`
	Examples              []*Example         `json:"examples"`
	ID                    string             `json:"id"`
	Notes                 []*CategorizedText `json:"notes"`
	Pronunciations        []*Pronunciation   `json:"pronunciations"`
	Regions               []string           `json:"regions"`
	Registers             []string           `json:"registers"`
	Subsenses             []*Sense           `json:"subsenses"`
	Translations          []*Translation     `json:"translations"`
	VariantForms          []*VariantForm     `json:"variantForms"`
}

type VariantForm struct {
	Regions []string `json:"regions"`
	Text    string   `json:"text"`
}

type CrossReference struct {
	ID   string `json:"id"`
	Text string `json:"text"`
	Type string `json:"type"`
}

type Example struct {
	Definitions  []string           `json:"definitions"`
	Domains      []string           `json:"domains"`
	Notes        []*CategorizedText `json:"notes"`
	Regions      []string           `json:"regions"`
	Registers    []string           `json:"registers"`
	SenseIDs     []string           `json:"senseIds"`
	Text         string             `json:"text"`
	Translations []*Translation     `json:"translations"`
}

type Translation struct {
	Domains             []string              `json:"domains"`
	GrammaticalFeatures []*GrammaticalFeature `json:"grammaticalFeatures"`
	Language            string                `json:"string"`
	Notes               []*CategorizedText    `json:"notes"`
	Regions             []string              `json:"regions"`
	Registers           []string              `json:"registers"`
	Text                string                `json:"text"`
}

type GetEntryOptions struct {
	// SourceLang     *string    `json:"source_lang"`
	// WordID         *string    `json:"word_id"`
}

func (s *EntryService) GetEntry(opt *GetEntryOptions, sourceLang string, wordId string) (*RetrieveEntry, *Response, error) {
	u := fmt.Sprintf("entries/%v/%v", sourceLang, wordId)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *RetrieveEntry
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
