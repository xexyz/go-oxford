package oxford

import (
	"fmt"
)

type UtilityService service

type Languages struct {
	Metadata *Metadata         `json:"metadata,omitempty"`
	Results  []*LanguagesEntry `json:"results"`
}

type LanguagesEntry struct {
	Region         string    `json:"region,omitempty"`
	Source         string    `json:"source,omitempty"`
	SourceLanguage *Language `json:"sourceLanguage,omitempty"`
	TargetLanguage *Language `json:"targetLanguage,omitempty"`
	Type           string    `json:"type,omitempty"`
}

type Language struct {
	ID       string `json:"id,omitempty"`
	Language string `json:"language,omitempty"`
}

type GetLanguagesOptions struct {
	SourceLanguage *string `url:"sourceLanguage,omitempty"`
	TargetLanguage *string `url:"targetLanguage,omitempty"`
}

type Filters struct {
	Metadata *Metadata       `json:"metadata,omitempty"`
	Results  []*FiltersEntry `json:"results,omitempty"`
}

type FiltersEntry struct {
	Entries      []string `json:"entries,omitempty"`
	Inflections  []string `json:"inflections,omitempty"`
	Translations []string `json:"translations,omitempty"`
	Wordlist     []string `json:"wordllist,omitempty"`
}

func (s *UtilityService) GetLanguages(opt *GetLanguagesOptions) (*Languages, *Response, error) {
	u := fmt.Sprintf("languages")
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *Languages
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *UtilityService) GetFilters(endpoint string) (*Filters, *Response, error) {
	u := fmt.Sprintf("filters/%v", endpoint)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var c *Filters
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
