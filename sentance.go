package oxford

import (
	"fmt"
)

type SentencesService service

type SentencesResults struct {
	Metadata *Metadata         `json:"metadata,omitempty"`
	Results  []*SentencesEntry `json:"results"`
}

type SentencesEntry struct {
	ID             string                   `json:"id"`
	Language       string                   `json:"language"`
	LexicalEntries []*SentencesLexicalEntry `json:"lexicalEntries"`
	Type           string                   `json:"type"`
	Word           string                   `json:"word"`
}

type SentencesLexicalEntry struct {
	GrammaticalFeatures []*GrammaticalFeature `json:"grammaticalFeatures"`
	Language            string                `json:"language"`
	LexicalCategory     string                `json:"lexicalCategory,omitempty"`
	Sentences           []*Example            `json:"sentences"`
	Text                string                `json:"text"`
}

func (s *SentencesService) GetSentences(sourceLang string, wordId string) (*SentencesResults, *Response, error) {
	u := fmt.Sprintf("entries/%v/%v", sourceLang, wordId)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var c *SentencesResults
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}
