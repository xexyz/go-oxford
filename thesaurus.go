package oxford

import (
	"fmt"
)

type ThesaurusService service

type Thesaurus struct {
	Metadata *Metadata            `json:"metadata,omitempty"`
	Results  []*HeadwordThesaurus `json:"results"`
}

type HeadwordThesaurus struct {
	ID             string                   `json:"id"`
	Language       string                   `json:"language"`
	LexicalEntries []*ThesaurusLexicalEntry `json:"lexicalEntries"`
	Type           string                   `json:"type"`
	Word           string                   `json:"word"`
}

type ThesaurusLexicalEntry struct {
	Entries         []*ThesaurusEntry `json:"entries"`
	Language        string            `json:"language"`
	LexicalCategory string            `json:"lexicalCategory"`
	Text            string            `json:"text"`
	VariantForms    []*VariantForm    `json:"variantForms"`
}

type ThesaurusEntry struct {
	HomographNumber string            `json:"homographNumber"`
	Senses          []*ThesaurusSense `json:"senses"`
	VariantForms    []*VariantForm    `json:"variantForms"`
}

type ThesaurusSense struct {
	Antonyms  []*SynonymAntonym `json:"antonyms"`
	Domains   []string          `json:"domains"`
	Examples  []*Example        `json:"examples"`
	ID        string            `json:"id"`
	Regions   []string          `json:"regions"`
	Registers []string          `json:"registers"`
	Subsenses []*ThesaurusSense `json:"subsenses"`
	Synonyms  []*SynonymAntonym `json:"synonyms"`
}

type SynonymAntonym struct {
	Domains   []string `json:"domains"`
	ID        string   `json:"id"`
	Language  string   `json:"language"`
	Regions   []string `json:"regions"`
	Registers []string `json:"registers"`
	Text      string   `json:"text"`
}

func (s *ThesaurusService) getThesaurusEntry(endpoint string, sourceLang string, wordId string) (*Thesaurus, *Response, error) {
	u := fmt.Sprintf("entries/%v/%v/%v", sourceLang, wordId, endpoint)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var c *Thesaurus
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

func (s *ThesaurusService) GetSynonyms(sourceLang string, wordId string) (*Thesaurus, *Response, error) {
	return s.getThesaurusEntry("synonyms", sourceLang, wordId)
}

func (s *ThesaurusService) GetAntonyms(sourceLang string, wordId string) (*Thesaurus, *Response, error) {
	return s.getThesaurusEntry("antonyms", sourceLang, wordId)
}

func (s *ThesaurusService) GetSynonymsAndAntonyms(sourceLang string, wordId string) (*Thesaurus, *Response, error) {
	return s.getThesaurusEntry("synonyms;antonyms", sourceLang, wordId)
}
