package oxford

import (
	"fmt"
)

type LemmatronService service

type Lemmatron struct {
	Metadata *Metadata            `json:"metadata,omitempty"`
	Results  []*HeadwordLemmatron `json:"results"`
}

type HeadwordLemmatron struct {
	ID             string                   `json:"id"`
	Language       string                   `json:"language"`
	LexicalEntries []*LemmatronLexicalEntry `json:"lexicalEntries"`
	Type           string                   `json:"type,omitempty"`
	Word           string                   `json:"word"`
}

type LemmatronLexicalEntry struct {
	GrammaticalFeatures []*GrammaticalFeature `json:"grammaticalFeatures,omitempty"`
	InflectionOfs       []*InflectionOf       `json:"inflectionOf"`
	Language            string                `json:"language"`
	LexicalCategory     string                `json:"lexicalCategory"`
	Text                string                `json:"text"`
}

type InflectionOf struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

func (s *LemmatronService) GetLemmatron(sourceLang string, wordId string) (*Lemmatron, *Response, error) {
	u := fmt.Sprintf("inflections/%v/%v", sourceLang, wordId)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var c *Lemmatron
	resp, err := s.client.Do(req, &c)
	if err != nil {
		return nil, resp, err
	}

	return c, resp, err
}

// func (s *LemmatronService) GetLemmatron(sourceLang string, wordId string, filters string) (*Lemmatron, *Response, error) {
// 	u := fmt.Sprintf("inflections/%v/%v/%v", sourceLang, wordId, filters)
// 	req, err := s.client.NewRequest("GET", u, nil)
// 	if err != nil {
// 		return nil, nil, err
// 	}

// 	var c *Lemmatron
// 	resp, err := s.client.Do(req, &c)
// 	if err != nil {
// 		return nil, resp, err
// 	}

// 	return c, resp, err
// }
