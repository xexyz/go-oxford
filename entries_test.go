package oxford

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"
)

func TestGetEntryService(t *testing.T) {
	mux, server, client := setup()
	defer teardown(server)

	mux.HandleFunc("/entries/en/test", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		fmt.Fprint(w, `{"id":1}`)
	})
	want := &RetrieveEntry{ID: 1}

	entry, _, err := client.Entries.GetEntry(nil, "en", "test")

	if err != nil {
		t.Fatalf("EntryService.GetEntry returns an error: %v", err)
	}

	if !reflect.DeepEqual(want, entry) {
		t.Errorf("EntryService.GetEntry returned %+v, want %+v", entry, want)
	}
}
